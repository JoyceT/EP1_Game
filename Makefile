
CC := g++
CFLAGS := -Wall

SRCFILES := $(wildcard scr/*.cpp)
INCFILES := $(wildcard inc/*.hpp)
DOCFILES := $(wildcard doc/*.txt)
OBJFILES := $(wildcard obj/*.o)
BINFILES := $(wildcard bin/saida)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	 $(CC) $(CFLAGS) obj/*.o -o bin/saida -lncurses

obj/%.o: src/%.cpp
		 $(CC) $(CFLAGS) -c $< -o $@ -I./inc -lncurses

.PHONY: clean
clean:
	rm -rf obj/*
	rm -rf bin/*

run:
	bin/saida




