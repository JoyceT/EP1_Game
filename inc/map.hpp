#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <string>


class Map {
	//Atributos
	protected:
		char range[20][76];

	public:
	//Construtores
		Map();
		
	//Acessores
		void setRange();
		void getRange();
		void addElemento(char sprite, int positionX, int positionY);	
};
#endif

		
