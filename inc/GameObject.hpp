#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <iostream>
#include <string>

class GameObject {
	//Atributos
	protected:
		int positionX;
		int positionY;
		char sprite;

	public:
		//Métodos Construtores
		GameObject();
		GameObject(char sprite, int positionX, int positionY);

		//Métodos Acessores
		void setPositionX(int valor);
		int getPositionX();
		void setPositionY(int valor);
		int getPositionY();
		void setSprite(char sprite);
		char getSprite();

		virtual void move();	//Diretiva virtual: subclasses subscrevem a classe mae

};
#endif
