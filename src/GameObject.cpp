#include <iostream>
#include <string>
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>
#include "GameObject.hpp"

using namespace std;

GameObject::GameObject(){}


GameObject::GameObject(char sprite, int positionX, int positionY){
	this->sprite = sprite;
	this->positionX = positionX;
	this->positionY = positionY;
}

void GameObject::setPositionX(int valor){
	this -> positionX += valor;
}

void GameObject::setPositionY(int valor){
	this -> positionY += valor;
}

void GameObject::setSprite(char sprite){
	this -> sprite = sprite;
}

int GameObject::getPositionX(){
	return this -> positionX;
}

int GameObject::getPositionY(){
	return this -> positionY;
}

char GameObject::getSprite(){
	return this -> sprite;
}

void GameObject::move(){

	char direction = 'l';

	direction = getch();

	if(direction == 'w'){
		this->setPositionY(1);
	} 
	else if (direction == 'z'){
		this->setPositionY(-1);
	} 
	else if (direction == 'a'){
		this->setPositionX(-1);
	} 
	else if (direction == 'd'){
		this->setPositionX(1);
	}
}

